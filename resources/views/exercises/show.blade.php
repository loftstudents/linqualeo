@extends('layouts.app')
@section('content')
<div id="page-wrapper">
    <h1>English tutor</h1>
    <div>

        <input type="button" value="new" id="runButton">
        <input type="button" value="shuffle" id="shuffleButton">

    </div>
    <div id="sourceBuffer"></div>
    <div id="displayArea"></div>
</div>
<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script src="js/main.js"></script>
@stop