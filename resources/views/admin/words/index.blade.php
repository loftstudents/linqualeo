@extends('layouts.app_admin')

@section('content')

    <div class="col-md-6">
        Добавить слово
            {{Form::open(array('url' => 'admin/words/store', 'method' => 'post'))}}
            {{Form::token()}}
            <input type="text" class="form-control" name="foreign">
            <br>
        Добавить перевод
        <input type="text" class="form-control" name="native">
        <br>
            <button type="submit" class="btn btn-success">Добавить</button>
        {{Form::close()}}
        <br>
        <table class="table">

            <th>id</th>
            <th>Cлово</th>
            <th>Перевод</th>
            <th>Управление</th>
            @foreach($list_words as $word)
            <tr>
                <td>{{$counter++}}</td>
                <td>{{$word->foreign}}</td>
                <td>{{$word->native}}</td>
                <td>
                    <a href="{{url('admin/words/destroy/'.$word->id)}}">Удалить</a>
                    <a href="{{url('admin/words/edit/'.$word->id)}}">Отредактировать</a>
                </td>
            </tr>
                @endforeach
        </table>
    </div>

@stop
