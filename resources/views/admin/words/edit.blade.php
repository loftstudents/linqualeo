@extends('layouts.app_admin')

@section('content')

    <div class="col-md-6">
            {{Form::open(array('url' => 'admin/words/update/'.$edit_words->id, 'method' => 'put'))}}
            {{Form::token()}}
            Слово
            <input type="text" class="form-control" value="{{$edit_words->foreign}}" name="foreign">
            <br>
            Перевод
            <input type="text" class="form-control" value="{{$edit_words->native}}" name="native">
            <br>
            <button type="submit" class="btn btn-success">Обновить</button>
            {{Form::close()}}<br>

    </div>

@stop
