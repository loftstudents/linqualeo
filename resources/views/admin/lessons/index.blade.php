@extends('layouts.app_admin')

@section('content')
    <a href="{{url('admin/lessons/create')}}"><span class="label label-primary">Добавить урок</span></a><br><br>
    <table class="table">
        <tr><th>№</th><th>Описание</th><th>Миссия</th><th>Order</th></tr>
        @foreach($lessons as $lesson)
            <tr><td>{{$counter++}}</td>
                <td>{{$lesson->description}}</td>
                <td>{{$lesson->mission->name}}</td>
                <td>{{$lesson->order}}</td>
                <td><a href="{{url('/admin/lessons/edit/'.$lesson->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                <td><a href="{{url('/admin/lessons/destroy/'.$lesson->id)}}"><span class="glyphicon glyphicon-remove-circle"></span></a></td>
            </tr>
        @endforeach
    </table>
@stop