@extends('layouts.app_admin')

@section('content')
    {{Form::open(array('url' => '/admin/lessons/store','method' => 'post'))}}
    {{Form::token()}}
    <label for="inputEmail3" class="col-sm-2 control-label">Описание урока</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="description"  rows="8"></textarea>
    </div><br><br><br>
    <p>Выбрать миссию</p>
    <select class="form-control" name="mission">
        @foreach($list_missions as $mission)

                <option value="{{$mission->id}}">{{$mission->name}}</option>

        @endforeach
    </select><br><br>
        <button type="submit" style="margin-left: 120px" class="btn btn-success">Обновить</button>
    {{ Form::close() }}
@stop
