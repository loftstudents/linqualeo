@extends('layouts.app_admin')

@section('content')
    {{Form::open(array('url' => '/admin/lessons/update/'.$edit_lessons->id,'method' => 'put'))}}
    {{Form::token()}}
    <label for="inputEmail3" class="col-sm-2 control-label">Описание урока</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="description"  rows="4">{{$edit_lessons->description}}</textarea>
    </div><br><br><br>
    <p>Выбрать миссию</p>
    <select class="form-control" name="mission">

        @foreach($list_mission as $mission)
        @if($mission->name==$edit_lessons->mission->name)
                <option value="{{$mission->id}}" selected>{{$mission->name}}</option>
            @else
            <option value="{{$mission->id}}">{{$mission->name}}</option>
            @endif
        @endforeach
    </select><br><br>
    <button type="submit" style="margin-left: 120px" class="btn btn-success">Обновить</button>
    {{ Form::close() }}
@stop
