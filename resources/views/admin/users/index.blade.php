@extends('layouts.app_admin')

@section('content')

    <table class="table">
        <tr><th>№</th><th>Имя</th><th>Email</th><th>Права</th></tr>users
        @foreach($users as $row_users)
            <tr><td>{{$counter++}}</td>
                <td>{{$row_users->name}}</td>
                <td>{{$row_users->email}}</td>
                <td>{{$row_users->role->description}}</td>
                <td><a href="{{url('/admin/users/edit/'.$row_users->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                <td><a href="{{url('/admin/users/delete/'.$row_users->id)}}"><span class="glyphicon glyphicon-remove-circle"></span></a></td>
            </tr>
        @endforeach
    </table>

@stop