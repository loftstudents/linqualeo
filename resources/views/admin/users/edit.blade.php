@extends('layouts.app_admin')

@section('content')
    {{Form::open(array('url' => '/admin/users/update/'.$user->id, 'method' => 'put'))}}
    {{Form::token()}}
    <label for="inputEmail3" class="col-sm-2 control-label">Имя</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{$user->name}}">
    </div><br><br><br>
    <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-10">
        <input type="email" class="form-control" name="email" value="{{$user->email}}">
    </div><br><br><br>
    <label for="inputEmail3" class="col-sm-2 control-label">Права</label>
    <div class="col-sm-10">
        <select class="form-control" name="role">
            @foreach($select_role as $role)
                @if($role->description==$user->role->description)
                    <option value="{{$user->role->id}}" selected>{{$role->description}}</option>

            @else
                    <option value="{{$user->role->id}}">{{$role->description}}</option>
                @endif
            @endforeach
        </select>
    </div>
    <button type="submit" class="btn btn-success">Обновить</button>
    {{ Form::close() }}

    @stop