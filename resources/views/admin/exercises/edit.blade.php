@extends('layouts.app_admin')

@section('content')
    {{Form::open(array('url' => '/admin/exercises/update/'.$exercises_edit->id, 'method' => 'put'))}}
    {{Form::token()}}
    <label for="inputEmail3" class="col-sm-2 control-label">Описание упражнения</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="description"  rows="">{{$exercises_edit->description}}</textarea>

    </div><br><br><br>
    <button type="submit" class="btn btn-success">Обновить</button>
    {{ Form::close() }}

@stop