@extends('layouts.app_admin')

@section('content')
    <a href="{{url('admin/exercises/create')}}"><span class="label label-primary">Добавить упражнение</span></a>
    <h5>Список упражнений</h5>
    <table class="table">
        <tr><th>№</th><th>Описание</th></tr>
        @foreach($exercises as $exercises_description)
            <tr><td>{{$counter++}}</td>
                <td>{{$exercises_description->description}}</td>
                <td><a href="{{url('/admin/exercises/edit/'.$exercises_description->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                <td><a href="{{url('/admin/exercises/delete/'.$exercises_description->id)}}"><span class="glyphicon glyphicon-remove-circle"></span></a></td>
            </tr>
        @endforeach
    </table>


@stop