@extends('layouts.app_admin')

@section('content')
    <p>Добавить упражнение</p>
    {{Form::open(array('url' => '/admin/exercises/store/', 'method' => 'post'))}}
    {{Form::token()}}
    <label for="inputEmail3" class="col-sm-2 control-label">Описание упражнения</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="description"  rows=""></textarea>

    </div><br><br><br>
    <button type="submit" class="btn btn-success">Добавить</button>
    {{ Form::close() }}

@stop