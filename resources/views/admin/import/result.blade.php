@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Результат импорта</h3>
                    </div>
                    <div class="panel-body">
                        Колличество слов добавленно: {{$count}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection