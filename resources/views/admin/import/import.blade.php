@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                @foreach($errors->all() as $error)
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Ошибка</h3>
                        </div>
                        <div class="panel-body">
                            {{$error}}
                        </div>
                    </div>
                @endforeach
                {!! Form::open(array('url' => 'admin/import', 'method'=>'post', 'enctype'=>'multipart/form-data', 'class'=>'form-inline')) !!}
                <div class="form-group">
                    <label class="sr-only" for="InputFile">Email address</label>
                    <input name='filename' type="file" class="form-control" id="InputFile">
                </div>
                <button type="submit" class="btn btn-default">Загрузить</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection