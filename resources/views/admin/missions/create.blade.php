@extends('layouts.app_admin')

@section('content')
    {{Form::open(array('url' => '/admin/missions/store',  'files' => true,'method' => 'post'))}}
    {{Form::token()}}
    <label for="inputEmail3" class="col-sm-2 control-label">Имя</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name">
    </div><br><br><br>
    <label for="inputEmail3" class="col-sm-2 control-label">Описание миссии</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="description"  rows="8"></textarea>
    </div><br><br><br>

    <p><input type="file" name="images"><br>
        <button type="submit" style="margin-left: 120px" class="btn btn-success">Обновить</button>
        {{ Form::close() }}
@stop
