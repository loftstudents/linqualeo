@extends('layouts.app_admin')

@section('content')
    <a href="{{url('admin/missions/create')}}"><span class="label label-primary">Добавить миссию</span></a><br><br>
    <table class="table">
        <tr><th>№</th><th>Имя</th><th>Описание</th></tr>
        @foreach($list_missions as $mission)
            <tr><td>{{$counter++}}</td>
                <td>{{$mission->name}}</td>
                <td>{{$mission->description}}</td>
                <td><a href="{{url('/admin/missions/edit/'.$mission->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                <td><a href="{{url('/admin/missions/delete/'.$mission->id)}}"><span class="glyphicon glyphicon-remove-circle"></span></a></td>
            </tr>
        @endforeach
    </table>
@stop