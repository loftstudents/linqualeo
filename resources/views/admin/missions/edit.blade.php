@extends('layouts.app_admin')

@section('content')
    {{Form::open(array('url' => '/admin/missions/update/'.$edit_missions->id,  'files' => true,'method' => 'put'))}}
    {{Form::token()}}
    <label for="inputEmail3" class="col-sm-2 control-label">Имя</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" name="name" value="{{$edit_missions->name}}">
    </div><br><br><br>
    <label for="inputEmail3" class="col-sm-2 control-label">Описание миссии</label>
    <div class="col-sm-10">
        <textarea class="form-control" name="description"  rows="8">{{$edit_missions->description}}</textarea>
    </div><br><br><br>

    <p><input type="file" name="images"><br>
    <button type="submit" style="margin-left: 120px" class="btn btn-success">Обновить</button>
    {{ Form::close() }}
    <br><br>
    @if($edit_missions->images)
    <ul class="nav nav-pills " style="margin-left: 140px">
        <li class="active">
            <a href="{{url('/admin/missions/destroy/images/'.$edit_missions->id)}}">
                <span class="badge pull-right">Удалить</span>
                <img src="{{$edit_missions->images}}" alt="Картинка миссии" height="100" width="100" class="img-thumbnail">
            </a>
        </li>
    </ul>
    @endif
    @stop
