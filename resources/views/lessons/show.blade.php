@extends('layouts.app')
@section('content')

    <div class="list-group">
        @for($i=0;$i<count($list_lessons);$i++)
        <a href="/exercises/{{$list_lessons[$i]->id}}" class="list-group-item">
         <span class="glyphicon glyphicon-star"></span>{{$list_lessons[$i]->description}}
        </a>
        @endfor
    </div>
@stop