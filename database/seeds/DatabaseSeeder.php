<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(RolesTableSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(MissionsTableSeeder::class);
        $this->call(LessonsTableSeeder::class);

    }
}
