<?php

use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lessons')->insert([
            'description' => 'Семья',
            'mission_id'  => '1',
        ]);
        DB::table('lessons')->insert([
            'description' => 'Животные',
            'mission_id'  => '1',
        ]);
        DB::table('lessons')->insert([
            'description' => 'Транспорт',
            'mission_id'  => '2',
        ]);
        DB::table('lessons')->insert([
            'description' => 'Истории',
            'mission_id'  => '3',
        ]);
    }
}
