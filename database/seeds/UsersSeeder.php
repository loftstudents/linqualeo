<?php

use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'bogdan',
            'email' => 'bzavoevanyy@gmail.com',
            'password' => bcrypt('secret'),
            'role_id' => 1
        ]);
    }
}
