<?php

use Illuminate\Database\Seeder;

class MissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('missions')->insert([
            'name' => 'Начальный уровень',
            'description' => 'Этот уровень стоит выбрать если вы новичок',
            'images' => '/images/missions/new_student.jpg',
        ]);
        DB::table('missions')->insert([
            'name' => 'Средний уровень',
            'description' => 'Для тех кто знает основы',
            'images' => '/images/missions/student.jpg',
        ]);
        DB::table('missions')->insert([
            'name' => 'Профессионал',
            'description' => 'Для глубокого изучения',
            'images' => '/images/missions/professional.jpg',
        ]);

    }
}
