<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LessonsFieldNameCorrecting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lessons', function ($table) {
            $table->dropForeign('lessons_id_mission_foreign');
            $table->renameColumn('id_mission', 'mission_id');
            $table->foreign('mission_id')->references('id')->on('missions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lessons', function ($table) {
            $table->dropForeign('lessons_mission_id_foreign');
            $table->renameColumn('mission_id', 'id_mission');
            $table->foreign('id_mission')->references('id')->on('missions');
        });
    }
}
