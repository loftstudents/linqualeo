<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysLessonExercises extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_exercises', function (Blueprint $table) {
            $table->foreign('id_lesson')->references('id')->on('lessons');
            $table->foreign('id_exercise')->references('id')->on('exercises');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_exercises', function (Blueprint $table) {
            $table->dropForeign('lesson_exercises_id_exercise_foreign');
            $table->dropForeign('lesson_exercises_id_lesson_foreign');
        });
    }
}
