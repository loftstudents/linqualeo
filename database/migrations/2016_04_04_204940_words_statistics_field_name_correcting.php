<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WordsStatisticsFieldNameCorrecting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('words_statistics', function (Blueprint $table) {
            $table->dropForeign('words_statistics_id_word_foreign');
            $table->renameColumn('id_word', 'word_id');
            $table->foreign('word_id')->references('id')->on('words');

            $table->dropForeign('words_statistics_id_user_foreign');
            $table->renameColumn('id_user', 'user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('words_statistics', function (Blueprint $table) {
            $table->dropForeign('words_statistics_word_id_foreign');
            $table->renameColumn('word_id', 'id_word');
            $table->foreign('id_word')->references('id')->on('words');

            $table->dropForeign('words_statistics_user_id_foreign');
            $table->renameColumn('user_id', 'id_user');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }
}
