<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LessonWordsFieldNameCorrecting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_words', function (Blueprint $table) {
            $table->dropForeign('lesson_words_id_lesson_foreign');
            $table->renameColumn('id_lesson', 'lesson_id');
            $table->foreign('lesson_id')->references('id')->on('lessons');

            $table->dropForeign('lesson_words_id_word_foreign');
            $table->renameColumn('id_word', 'word_id');
            $table->foreign('word_id')->references('id')->on('words');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_words', function (Blueprint $table) {
            $table->dropForeign('lesson_words_lesson_id_foreign');
            $table->renameColumn('lesson_id', 'id_lesson');
            $table->foreign('id_lesson')->references('id')->on('lessons');

            $table->dropForeign('lesson_words_word_id_foreign');
            $table->renameColumn('word_id', 'id_word');
            $table->foreign('id_word')->references('id')->on('words');
        });
    }
}
