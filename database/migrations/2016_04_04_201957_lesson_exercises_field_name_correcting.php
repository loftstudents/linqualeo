<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LessonExercisesFieldNameCorrecting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_exercises', function (Blueprint $table) {
            $table->dropForeign('lesson_exercises_id_exercise_foreign');
            $table->renameColumn('id_exercise', 'exercise_id');
            $table->foreign('exercise_id')->references('id')->on('exercises');

            $table->dropForeign('lesson_exercises_id_lesson_foreign');
            $table->renameColumn('id_lesson', 'lesson_id');
            $table->foreign('lesson_id')->references('id')->on('lessons');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_exercises', function (Blueprint $table) {
            $table->dropForeign('lesson_exercises_exercise_id_foreign');
            $table->renameColumn('exercise_id', 'id_exercise');
            $table->foreign('id_exercise')->references('id')->on('exercises');

            $table->dropForeign('lesson_exercises_lesson_id_foreign');
            $table->renameColumn('lesson_id', 'id_lesson');
            $table->foreign('id_lesson')->references('id')->on('lessons');
        });
    }
}
