<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeysLessonWords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lesson_words', function (Blueprint $table) {
            $table->foreign('id_lesson')->references('id')->on('lessons');
            $table->foreign('id_word')->references('id')->on('words');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lesson_words', function (Blueprint $table) {
            $table->dropForeign('lesson_words_id_lesson_foreign');
            $table->dropForeign('lesson_words_id_word_foreign');
        });
    }
}
