<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MissionsStatisticsFieldNameCorrecting extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('missions_statistics', function (Blueprint $table) {
            $table->dropForeign('missions_statistics_id_mission_foreign');
            $table->renameColumn('id_mission', 'mission_id');
            $table->foreign('mission_id')->references('id')->on('missions');

            $table->dropForeign('missions_statistics_id_user_foreign');
            $table->renameColumn('id_user', 'user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('missions_statistics', function (Blueprint $table) {
            $table->dropForeign('missions_statistics_mission_id_foreign');
            $table->renameColumn('mission_id', 'id_mission');
            $table->foreign('id_mission')->references('id')->on('missions');

            $table->dropForeign('missions_statistics_user_id_foreign');
            $table->renameColumn('user_id', 'id_user');
            $table->foreign('id_user')->references('id')->on('users');
        });
    }
}
