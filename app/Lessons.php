<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lessons extends Model
{
    public $timestamps = false;
    public function mission()
    {
        return $this->belongsTo('App\Missions','mission_id');
    }
}
