<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::group(['middleware' => ['web']], function () {
//    Route::auth();
//    Route::get('/', function () {
//        return view('welcome');
//    });
//
//});

Route::group(['middleware' => ['web'/*,'auth'*/]], function () {
    Route::auth();
    Route::get('/', function () {
        return view('welcome');
    });
    Route::group(['middleware'=>['admin']],function(){
        Route::group(['prefix' => 'admin'], function(){
            Route::get('/', 'HomeController@indexAdmin');
            Route::get('/users', 'UsersController@index');
            Route::get('/words', 'WordsController@index');
            Route::get('/words/destroy/{id}', 'WordsController@destroy');
            Route::get('/words/edit/{id}', 'WordsController@edit');
            Route::post('/words/store', 'WordsController@store');
            Route::put('/words/update/{id}', 'WordsController@update');
            Route::get('/exercises', 'ExercisesController@index');
            Route::get('/lessons', 'LessonController@indexAdmin');
            Route::get('/lessons/destroy/{id}', 'LessonController@destroy');
            Route::get('/lessons/create/', 'LessonController@create');
            Route::post('/lessons/store/', 'LessonController@store');
            Route::get('/lessons/edit/{id}', 'LessonController@edit');
            Route::put('/lessons/update/{id}', 'LessonController@update');

            Route::get('/exercises/delete/{id}','ExercisesController@destroy');
            Route::get('/exercises/edit/{id}','ExercisesController@edit');
            Route::put('/exercises/update/{id}','ExercisesController@update');
            Route::get('/exercises/create','ExercisesController@create');
            Route::post('/exercises/store','ExercisesController@store');
            Route::get('/missions', 'MissionsController@indexAdmin');
            Route::get('/missions/delete/{id}','MissionsController@destroy');
            Route::get('/missions/edit/{id}','MissionsController@edit');
            Route::put('/missions/update/{id}','MissionsController@update');
            Route::get('/missions/create','MissionsController@create');
            Route::post('/missions/store','MissionsController@store');
            Route::get('/missions/destroy/images/{id}','MissionsController@destroyImages');
            Route::get('/statistic', 'StudentStatisticController@index');
            Route::get('/import', 'ImportController@index');
            Route::post('/import', 'ImportController@import');
            Route::get('/users/delete/{id}','UsersController@destroy');
            Route::get('/users/edit/{id}','UsersController@edit');
            Route::put('/users/update/{id}','UsersController@update');
        });
    });
    Route::resource('lesson','LessonController');
    Route::resource('exercises','ExercisesController');
    Route::resource('missions','MissionsController');
    Route::get('/home', 'HomeController@index');
    Route::get('/404',function ()
    {
    return view('404.404');
    });
});


