<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use App\Roles;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(!Auth::check())
        {

            return redirect('/404');
        }
        if (Roles::find(Auth::user()->role_id)->description != 'admin')
        {
            return redirect()->route('404');
        }




        return $next($request);
    }
}
