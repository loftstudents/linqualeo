<?php

namespace App\Http\Controllers;

use App\Exercises;
use Illuminate\Http\Request;

use App\Http\Requests;

class ExercisesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['exercises'] = Exercises::all();
        $data['counter']   = 1;
        return view('admin/exercises/index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.exercises/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'description'  => 'required|max:255',
        ]);
        $new_exercises = new Exercises();
        $new_exercises->description = $request->description;
        $new_exercises->save();
        return redirect('admin/exercises');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('exercises.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['exercises_edit']   =   Exercises::find($id);
        return view('admin.exercises.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'description'        => 'required|max:255',
        ]);
        $update_exercises                = Exercises::find($id);
        $update_exercises->description   = $request->description;
        $update_exercises->save();
        return redirect('/admin/exercises');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exercises::destroy($id);
        return redirect('/admin/exercises');
    }
}
