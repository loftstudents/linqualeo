<?php

namespace App\Http\Controllers;

use App\Missions;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
class MissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     * Mission display which sets of lessons enter
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['list_missions']       = Missions::all();
        return view('missions.index',$data);
    }

    public function indexAdmin()
    {
        $data['list_missions']       = Missions::all();
        $data['counter']             = 1;

        return view('admin.missions.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.missions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'        => 'required|max:255',
            'description' => 'required|max:255',
            'images'      => 'mimes:jpeg,bmp,png,jpg|max:10000',
        ]);

        $images                       = $this->workWithImages($request);
        $new_missions                 = new Missions();
        $new_missions->name           = $request->name;
        $new_missions->description    = $request->description;
        $images?$new_missions->images = $images :true;
        $new_missions->save();
        return redirect('/admin/missions');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $data['edit_missions']   = Missions::find($id);
        return view('admin.missions.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'        => 'required|max:255',
            'description' => 'required|max:255',
            'images'      => 'mimes:jpeg,bmp,png|max:10000',
        ]);
        $image                          = $this->workWithImages($request);
        $update_missions                = Missions::find($id);
        $update_missions->name          = $request->name;
        $update_missions->description   = $request->description;
        $image?$update_missions->images = $image:true;
        $update_missions->save();
        return redirect('/admin/missions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Missions::destroy($id);
        return redirect('/admin/missions');
    }

    /**
     * to delete the picture of missions
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroyImages($id)
    {
        $destroy_images           = Missions::find($id);
        $destroy_images->images   = '';
        $destroy_images->save();
        return redirect()->back();
    }

    /**
     * edits and returns the image if it was loaded
     * @param Request $request
     * @return bool|string
     */
    protected function workWithImages(Request $request)
   {
       //Открываем файл
       $image         = Input::file('images');
       //Работаем с изображением если только загружено
       if(!empty($image))
       {
           $filename               = $request->name.'.'.$image->getClientOriginalExtension();
           $path_for_save          = public_path('/images/missions/' . $filename);
           $path_for_save_table    = '/images/missions/' . $filename;
           Image::make($image->getRealPath())->resize(200, 200)->save($path_for_save);
           return $path_for_save_table;
       }else{
           return false;
       }
   }
}
