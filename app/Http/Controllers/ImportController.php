<?php

namespace App\Http\Controllers;

use App\Words;
use Illuminate\Http\Request;
use PHPExcel_IOFactory;
use App\Http\Requests;
use Validator;

class ImportController extends Controller
{
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    public function index()
    {
        return view('admin.import.import');
    }

    public function import(Request $request)
    {

        $validator = Validator::make($request->file(), [
            'size' => '2048',
            'filename' => 'mimes:xl,xlsx,dotx',
        ]);

        if ($validator->fails()) {
            return redirect('admin/import')
                ->withErrors($validator)
                ->withInput();
        }
        $file = $request->file('filename')->getRealPath();

        $inputFileType = PHPExcel_IOFactory::identify($file);
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objReader->setReadDataOnly(true);
        $objPHPExcel = $objReader->load($file);
        $objPHPExcel->setActiveSheetIndex(0);
        $aSheet = $objPHPExcel->getActiveSheet();

        $array = array();

        //получим итератор строки и пройдемся по нему циклом

        foreach ($aSheet->getRowIterator() as $row) {
            //получим итератор ячеек текущей строки

            $cellIterator = $row->getCellIterator();

            //пройдемся циклом по ячейкам строки
            //этот массив будет содержать значения каждой отдельной строки

            $item = array();

            foreach ($cellIterator as $cell) {
                //заносим значения ячеек одной строки в отдельный массив

                array_push($item, $cell->getCalculatedValue());
            }

            //заносим массив со значениями ячеек отдельной строки в "общий массв строк"

            array_push($array, $item);
        }

        array_shift($array);


        $count = 0;
        foreach ($array as $value)
        {
            $words = new Words();
            $temp = $words->where('native', '=', $value[0])->get()->toArray();
            if (empty($temp))
            {
                $count++;
                $words->native = $value[0];
                $words->foreign = $value[1];
                $words->lang_native = $value[2];
                $words->lang_foreign = $value[3];
                $words->save();
            }
        }
        $data['count'] = $count;
        return view('admin.import.result', $data);
    }
}
