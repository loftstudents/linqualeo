<?php

namespace App\Http\Controllers;
use App\Words;
use Illuminate\Http\Request;

use App\Http\Requests;

class WordsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['counter']     =   1;
        $data['list_words']  =   Words::all();
        return view('admin/words.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $new_words          = new Words();
        $new_words->foreign = $request->foreign;
        $new_words->native  = $request->native;
        $new_words->save();
        return redirect('admin/words');
        dd($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['edit_words'] = Words::find($id);
        return view('admin.words.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $update_word          = Words::find($id);
        $update_word->native  = $request->native;
        $update_word->foreign = $request->foreign;
        $update_word->save();
        return redirect('admin/words');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Words::destroy($id);
        return redirect('admin/words');
    }
}
