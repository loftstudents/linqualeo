<?php

namespace App\Http\Controllers;

use App\Lessons;
use App\Missions;
use Illuminate\Http\Request;

use App\Http\Requests;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/lessons');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexAdmin()
    {

        $data['lessons'] = Lessons::with('mission')->get();

        $data['counter'] = 1;
        return view('admin.lessons.index',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['list_missions'] = Missions::all();

       return view('admin.lessons.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'description'=> 'required|max:255',
            'mission'   => 'required|numeric'
        ]);
        $new_lessons                 = new Lessons();
        $new_lessons->description    = $request->description;
        $new_lessons->mission_id     = $request->mission;
        $new_lessons->save();
        return redirect('admin/lessons');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['list_lessons'] = Lessons::where('mission_id', '=', $id)->get();
        return view('lessons.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['edit_lessons'] = Lessons::with('mission')->find($id);
        $data['list_mission'] = Missions::all();
//        dd($data['list_mission']);
        return view('admin.lessons.edit',$data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'description'=> 'required|max:255',
             'mission'   => 'required|numeric'
        ]);
        $update_lessons = Lessons::find($id);
        $update_lessons->description = $request->description;
        $update_lessons->id_mission  = $request->mission;
        $update_lessons->save();
        return redirect('admin/lessons');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lessons::destroy($id);
        return redirect('/admin/lessons');

    }

}
